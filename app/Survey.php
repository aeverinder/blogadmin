<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    /*
|--------------------------------------------------------------------------
| Survey model
|--------------------------------------------------------------------------
|
| This section of the model is where the fields that can be filled will be
| decided
|
|
*/
    protected $fillable = [
        'title',
        'content',
        'question_1',
        'answer_1',
        'answer_2',
        'answer_3',
        'question_2',
        'answer_4',
        'answer_5',
        'answer_6',
        'question_3',
        'answer_7',
        'answer_8',
        'answer_9',
        'question_4',
        'answer_10',
        'answer_11',
        'answer_12',
        'question_5',
        'answer_13',
        'answer_14',
        'answer_15',



    ];

    /**
     * Get the categories associated with the given article.
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    /**
     * Get the user associated with the given article
     *
     * @return mixed
     */
    public function author()
    {
        return $this->belongsTo('App\User', 'author_id');
    }
}