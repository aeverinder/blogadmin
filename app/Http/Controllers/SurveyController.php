<?php

namespace App\Http\Controllers;

use App\Category;
use App\Survey;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Article;
use Illuminate\Support\Facades\Auth;

class SurveyController extends Controller
{
    /*
 * Secure the set of pages to the admin.
 */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all the surveys
        $surveys = Survey::all();

        return view('admin.surveys', ['surveys' => $surveys]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats = Category::pluck('title', 'id');

        // now we can return the data with the view
        return view('admin/surveys/create', compact('cats'));

    }
    public function store(Request $request)
    {
        $survey = Survey::create($request->all());
        $survey->categories()->attach($request->input('category'));
        $survey->author_id = Auth::user()->id;
        $survey->save();

        return redirect('admin/surveys');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        // get the article
        $survey = Survey::where('id',$id)->first();

        // if article does not exist return to list
        if(!$survey)
        {
            return redirect('/admin/surveys'); // you could add on here the flash messaging of article does not exist.
        }
        return view('/admin/surveys/show')->withSurvey($survey);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}