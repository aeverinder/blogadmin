@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Questionnaire review</div>

                    <div class="panel-body">
                        {{--Pulling the title data from the survey title field in the survey table--}}
                        <h1>{{ $survey->title }}</h1>
                        {{--Pulling the content data from the survey title field in the survey table--}}
                        <p> Content: {{ $survey->content }}</p>

                        {{--Opening up the form and pulling the store function from the results controller--}}
                        {!! Form::open(array('action' => 'ResultsController@store', 'id' => 'resultarticle')) !!}

                        <p></p>

                        {{--Pulling the question 1 data from the survey title field in the survey table--}}
                        <h3> {{ $survey->question_1 }}</h3>
                        possible answers:
                        {{--Creating an input field. The radio command only allows them to select from the
                        data that has been pulled from the database. --}}
                        {{--The 'value =' section at the bottom of the section is where the information
                        related to the answer will be pulled from the database and presented to
                         the user--}}
                        <input type="radio" name="answer_1"
                               <?php if (isset($answer_1) && $answer_1==$answer_1) echo "checked";?>
                               value={!! $survey->answer_1 !!}>{!! $survey->answer_1 !!}

                        <input type="radio" name="answer_1"
                               <?php if (isset($answer_2) && $answer_2==$answer_2) echo "checked";?>
                               value={!! $survey->answer_2 !!}>{!! $survey->answer_2 !!}

                        <input type="radio" name="answer_1"
                               <?php if (isset($answer_3) && $answer_3==$answer_3) echo "checked";?>
                               value={!! $survey->answer_3 !!}>{!! $survey->answer_3 !!}
                        <p></p>

                        {{--Pulling the question 2 data from the survey title field in the survey table--}}
                        <h3> {{ $survey->question_2 }}</h3>
                        possible answers:
                        {{--Creating an input field. The radio command only allows them to select from the
                        data that has been pulled from the database. --}}
                        {{--The 'value =' section at the bottom of the section is where the information
                        related to the answer will be pulled from the database and presented to
                         the user--}}
                        <input type="radio" name="answer_2"
                               <?php if (isset($answer_4) && $answer_4==$answer_4) echo "checked";?>
                               value={!! $survey->answer_4 !!}>{!! $survey->answer_4 !!}

                        <input type="radio" name="answer_2"
                               <?php if (isset($answer_5) && $answer_5==$answer_5) echo "checked";?>
                               value={!! $survey->answer_5 !!}>{!! $survey->answer_5 !!}

                        <input type="radio" name="answer_2"
                               <?php if (isset($answer_6) && $answer_6==$answer_6) echo "checked";?>
                               value={!! $survey->answer_6 !!}>{!! $survey->answer_6 !!}

                        {{--Pulling the question 3 data from the survey title field in the survey table--}}
                        <h3> {{ $survey->question_3 }}</h3>
                        possible answers:
                        {{--Creating an input field. The radio command only allows them to select from the
                        data that has been pulled from the database. --}}
                        {{--The 'value =' section at the bottom of the section is where the information
                        related to the answer will be pulled from the database and presented to
                         the user--}}
                        <input type="radio" name="answer_3"
                               <?php if (isset($answer_7) && $answer_7==$answer_7) echo "checked";?>
                               value={!! $survey->answer_7 !!}>{!! $survey->answer_7 !!}

                        <input type="radio" name="answer_3"
                               <?php if (isset($answer_8) && $answer_8==$answer_8) echo "checked";?>
                               value={!! $survey->answer_8 !!}>{!! $survey->answer_8 !!}
                        <input type="radio" name="answer_3"
                               <?php if (isset($answer_9) && $answer_9==$answer_9) echo "checked";?>
                               value={!! $survey->answer_9 !!}>{!! $survey->answer_9 !!}

                        {{--Pulling the question 4 data from the survey title field in the survey table--}}
                        <h3> {{ $survey->question_4 }}</h3>
                        possible answers:
                        {{--Creating an input field. The radio command only allows them to select from the
                        data that has been pulled from the database. --}}
                        {{--The 'value =' section at the bottom of the section is where the information
                        related to the answer will be pulled from the database and presented to
                         the user--}}
                        <input type="radio" name="answer_4"
                               <?php if (isset($answer_10) && $answer_10==$answer_10) echo "checked";?>
                               value={!! $survey->answer_10 !!}>{!! $survey->answer_10 !!}
                        <input type="radio" name="answer_4"
                               <?php if (isset($answer_11) && $answer_11==$answer_11) echo "checked";?>
                               value={!! $survey->answer_11 !!}>{!! $survey->answer_11 !!}
                        <input type="radio" name="answer_4"
                               <?php if (isset($answer_12) && $answer_12==$answer_12) echo "checked";?>
                               value={!! $survey->answer_12 !!}>{!! $survey->answer_12 !!}

                        {{--Pulling the question 5 data from the survey title field in the survey table--}}
                        <h3> {{ $survey->question_5 }}</h3>
                        possible answers:
                        {{--Creating an input field. The radio command only allows them to select from the
                        data that has been pulled from the database. --}}
                        {{--The 'value =' section at the bottom of the section is where the information
                        related to the answer will be pulled from the database and presented to
                         the user--}}
                        <input type="radio" name="answers_5"
                               <?php if (isset($answer_13) && $answer_13==$answer_13) echo "checked";?>
                               value={!! $survey->answer_13 !!}>{!! $survey->answer_13 !!}
                        <input type="radio" name="answer_5"
                               <?php if (isset($answer_14) && $answer_14==$answer_14) echo "checked";?>
                               value={!! $survey->answer_14 !!}>{!! $survey->answer_14 !!}
                        <input type="radio" name="answer_5"
                               <?php if (isset($answer_15) && $answer_15==$answer_15) echo "checked";?>
                               value={!! $survey->answer_15 !!}>{!! $survey->answer_15 !!}
                        <input type="hidden" name="questionnaire_id" value="{{ $survey->id }}">

                        <p></p>
                        {{--Pulling the creator from the survey table in the database--}}
                        <p>creator: {{$survey->author->name}}</p>
                        {{--The form submit button for the show view--}}
                        <div class="row large-4 columns">
                            {!! Form::submit('End questionnaire', ['class' => 'button']) !!}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection