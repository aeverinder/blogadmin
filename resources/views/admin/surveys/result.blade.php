@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">View questionnaire results </div>
                    <div class="panel-body">
                        {{--This section of the code is the section that will show the the results of a questionaire in the view--}}
                        <section>
                        {{--This section of the code makes it so if the results are there then the form will be presented showing the results--}}
                        @if(isset($results))
                            @foreach($results as $result)
                                {{--This section pulls the questionnaire id data from the results table in the database--}}
                                <p>Questionnaire number: {{$result->questionnaire_id}}</p>
                                    {{--This section pulls the id data from the results table in the database--}}
                                    <p>Person to answer:  {{$result->id}}</p>
                                    {{--This section pulls the answer 1 data from the results table in the database--}}
                                    <p> Answer to question 1:{{ $result->answer_1 }}</p>
                                    {{--This section pulls the answer 2 data from the results table in the database--}}
                                    <p>Answer to question 2:{{ $result->answer_2 }}</p>
                                    {{--This section pulls the answer 3 data from the results table in the database--}}
                                    <p>Answer to question 3:{{ $result->answer_3 }}</p>
                                    {{--This section pulls the answer 4 data from the results table in the database--}}
                                    <p>Answer to question 4:{{ $result->answer_4 }}</p>
                                    {{--This section pulls the answer 5 data from the results table in the database--}}
                                    <p>Answer to question 5:{{ $result->answer_5 }}</p>

                                @endforeach
                        @endif
                        </section>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection