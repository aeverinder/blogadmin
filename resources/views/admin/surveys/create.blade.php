@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Create Questionnaire</div>

                    <div class="panel-body">



                        {{--Opening up the form and pulling the store function from the survey controller--}}
                        {!! Form::open(array('action' => 'SurveyController@store', 'id' => 'createsurvey')) !!}

                        {{--Allowing the user to type in their desired title for the questionnaire.
                        This will also then be stored in the database--}}
                        <div class="row large-12 columns">
                            {!! Form::label('title', 'Title:') !!}
                            {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        {{--Allowing the user to type in their desired content for the questionnaire.
                        This will also then be stored in the database--}}
                        <div class="row large-12 columns">
                            {!! Form::label('content', 'Detail:') !!}
                            {!! Form::text('content', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        {{--Allowing the user to type in their desired Question for the questionnaire.
                        This will also then be stored in the database--}}
                        <div class="row large-12 columns">
                            {!! Form::label('question_1', 'Question 1:') !!}
                            {!! Form::text('question_1', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        {{--Allowing the user to type in their desired answer options for the question 1.
                        These will also then be stored in the database--}}
                        possible answers:
                        <div class="row large-12 columns">
                            {!! Form::label('answer_1', 'Possible answer 1:') !!}
                            {!! Form::text('answer_1', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('answer_2', 'Possible answer 2:') !!}
                            {!! Form::text('answer_2', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('answer_3', 'Possible answer 3:') !!}
                            {!! Form::text('answer_3', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        {{--Allowing the user to type in their desired Question for the questionnaire.
                        This will also then be stored in the database--}}
                        <div class="row large-12 columns">
                            {!! Form::label('question_2', 'Question 2:') !!}
                            {!! Form::text('question_2', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        {{--Allowing the user to type in their desired answer options for the question 2.
                        These will also then be stored in the database--}}
                        possible answers:
                        <div class="row large-12 columns">
                            {!! Form::label('answer_4', 'Possible answer 1:') !!}
                            {!! Form::text('answer_4', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('answer_5', 'Possible answer 2:') !!}
                            {!! Form::text('answer_5', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('answer_6', 'Possible answer 3:') !!}
                            {!! Form::text('answer_6', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        {{--Allowing the user to type in their desired Question for the questionnaire.
                        This will also then be stored in the database--}}
                        <div class="row large-12 columns">
                            {!! Form::label('question_3', 'Question 3:') !!}
                            {!! Form::text('question_3', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        {{--Allowing the user to type in their desired answer options for the question 3.
                        These will also then be stored in the database--}}
                        possible answers:
                        <div class="row large-12 columns">
                            {!! Form::label('answer_7', 'Possible answer 1:') !!}
                            {!! Form::text('answer_7', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('answer_8', 'Possible answer 2:') !!}
                            {!! Form::text('answer_8', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('answer_9', 'Possible answer 3:') !!}
                            {!! Form::text('answer_9', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        {{--Allowing the user to type in their desired Question for the questionnaire.
                        This will also then be stored in the database--}}
                        <div class="row large-12 columns">
                            {!! Form::label('question_4', 'Question 4:') !!}
                            {!! Form::text('question_4', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        {{--Allowing the user to type in their desired answer options for the question 4.
                        These will also then be stored in the database--}}
                        possible answers:
                        <div class="row large-12 columns">
                            {!! Form::label('answer_10', 'Possible answer 1:') !!}
                            {!! Form::text('answer_10', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('answer_11', 'Possible answer 2:') !!}
                            {!! Form::text('answer_11', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('answer_12', 'Possible answer 3:') !!}
                            {!! Form::text('answer_12', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        {{--Allowing the user to type in their desired Question for the questionnaire.
                        This will also then be stored in the database--}}
                        <div class="row large-12 columns">
                            {!! Form::label('question_5', 'Question 5:') !!}
                            {!! Form::text('question_5', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        {{--Allowing the user to type in their desired answer options for the question 5.
                        These will also then be stored in the database--}}
                        possible answers:
                        <div class="row large-12 columns">
                            {!! Form::label('answer_13', 'Possible answer 1:') !!}
                            {!! Form::text('answer_13', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('answer_14', 'Possible answer 2:') !!}
                            {!! Form::text('answer_14', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('answer_15', 'Possible answer 3:') !!}
                            {!! Form::text('answer_15', null, ['class' => 'large-8 columns']) !!}
                        </div>


                        {{--This submits the questionnaire --}}
                        <div class="row large-4 columns">
                            {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}
                        </div>

                        {{--Closing the form--}}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
