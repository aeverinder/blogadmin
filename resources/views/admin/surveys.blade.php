@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Questionnaires</div>

                    <div class="panel-body">
                        Questionnaires ready to answer
                        <section>
                            @if (isset ($surveys))
                                <ul>
                                    @foreach ($surveys as $survey)
                                        <li><a href="/admin/surveys/{{ $survey->id }}" name="{{ $survey->title }}">{{ $survey->title }}</a></li>
                                        <td><a href="/admin/surveys/result/{{  $survey->id }}" name="{{ $survey->title }}">view Results</a></td>
                                    @endforeach

                                </ul>
                            @else
                                <p> no articles added yet </p>
                            @endif
                        </section>

                        {{ Form::open(array('action' => 'SurveyController@create', 'method' => 'get')) }}
                        <div class="row">
                            {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}
                        </div>
                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection