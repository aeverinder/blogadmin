<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
 /*
|--------------------------------------------------------------------------
| Result migration - Field names and field types
|--------------------------------------------------------------------------
|
| This section of the migrations table is where the fields for the database are
| made and where the field types are defined.
|
| As well as defining the field names and types this section also has a foreign
| which links is linked using the references id on survey line. This line means
| that the results table will pull the questionnaire id from the survey table.
*/
        Schema::create('results', function (Blueprint $table) {
            $table->integer('questionnaire_id')->unsigned();
            $table->foreign('questionnaire_id')->references('id')->on('surveys')->onDelete('cascade');
            $table->increments('id');
            $table->string('title');
            $table->string('content');
            $table->string('answer_1');
            $table->string('answer_2');
            $table->string('answer_3');
            $table->string('answer_4');
            $table->string('answer_5');
            $table->integer('author_id')->unsigned()->default(0);
            //$table->foreign('author')->refernces('id')->on('users')->onDelete('cascade');
            $table->timestamps();
            $table->string('published_at')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('results');
    }
}