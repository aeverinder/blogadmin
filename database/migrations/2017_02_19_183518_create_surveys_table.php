<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
 /*
|--------------------------------------------------------------------------
| Survey migration - Field names and field types
|--------------------------------------------------------------------------
|
| This section of the migrations table is where the fields for the database are
| made and where the field types are defined.
*/
        Schema::create('surveys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('content');
            $table->string('question_1');
            $table->string('answer_1');
            $table->string('answer_2');
            $table->string('answer_3');
            $table->string('question_2');
            $table->string('answer_4');
            $table->string('answer_5');
            $table->string('answer_6');
            $table->string('question_3');
            $table->string('answer_7');
            $table->string('answer_8');
            $table->string('answer_9');
            $table->string('question_4');
            $table->string('answer_10');
            $table->string('answer_11');
            $table->string('answer_12');
            $table->string('question_5');
            $table->string('answer_13');
            $table->string('answer_14');
            $table->string('answer_15');
            $table->integer('author_id')->unsigned()->default(0);
            //$table->foreign('author')->refernces('id')->on('users')->onDelete('cascade');
            $table->timestamps();
            $table->string('published_at')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('surveys');
    }
}