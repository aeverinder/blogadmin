<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    /*
|--------------------------------------------------------------------------
| Result model
|--------------------------------------------------------------------------
|
| This section of the model is where the fields that can be filled will be
| decided
|
|
*/
    protected $fillable = [
        'title',
        'questionnaire_id',
        'content',
        'answer_1',
        'answer_2',
        'answer_3',
        'answer_4',
        'answer_5',


    ];

    /*
|--------------------------------------------------------------------------
| Result model public function
|--------------------------------------------------------------------------
|
| This public function shows how this model belongs to the survey model
*/



    public function surveys()
    {
        return $this->belongsTo('App\Survey');
    }
}
