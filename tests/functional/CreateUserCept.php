<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('create a new user');

// When
$I->amOnPage('/admin/users');
$I->see('Users', 'h1');
$I->dontSee('Alex Verinder');
// And
$I->click('Add User');

// Then
$I->amOnPage('/admin/users/create');
// And
$I->see('Add User', 'h1');
$I->submitForm('.createuser', [
    'name' => 'Alex Verinder',
    'email' => 'alexander.verinder@go.edgehill.ac.uk',
    'password' => 'password'
]);
// Then
$I->seeCurrentUrlEquals('/admin/users');
$I->see('Users', 'h1');
$I->see('New user added!');
$I->see('Alex Verinder');



// Check for duplicates

// When
$I->amOnPage('/admin/users');
$I->see('Users', 'h1');
$I->see('Alex Verinder');
// And
$I->click('Add User');

// Then
$I->amOnPage('/admin/users/create');
// And
$I->see('Add User', 'h1');
$I->submitForm('.createuser', [
    'name' => 'Alex Verinder',
    'email' => 'alexvander.verinder@go.edgehill.ac.uk',
    'password' => 'password'
]);
// Then
$I->seeCurrentUrlEquals('/admin/users');
$I->see('Users', 'h1');
$I->see('Error user already exists!');

