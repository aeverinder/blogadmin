<?php

namespace App\Http\Controllers;

use
    Illuminate\Http\Request;
use App\Result;
use App\Survey;
use App\Http\Requests;

class ResultsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //get all the surveys
        //$result = Result::all();

        return view('admin.surveys.result');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats = Category::pluck('title', 'id');

        // now we can return the data with the view
        return view('admin/surveys/create', compact('cats'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $article = Result::create($request->all());
//        $article->categories()->attach($request->input('category'));
//        $article->author_id = Auth::user()->id;
//        $article->save();

        return redirect('admin/surveys');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $surveys = Survey::findOrFail($id);
        $results = Result::where('questionnaire_id', $id)->get();
        return view('admin.surveys.result', ['surveys' => $surveys, 'results' => $results]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}