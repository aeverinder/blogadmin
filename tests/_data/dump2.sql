-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: blogadmin
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article_category`
--

DROP TABLE IF EXISTS `article_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_category` (
  `article_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_category`
--

LOCK TABLES `article_category` WRITE;
/*!40000 ALTER TABLE `article_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `article_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `published_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `articles_published_at_index` (`published_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_title_unique` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'HTML','html',NULL,NULL),(2,'PHP','php',NULL,NULL),(3,'CSS','css',NULL,NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2017_02_19_183133_create_roles_table',1),('2017_02_19_183350_create_permissions_table',1),('2017_02_19_183518_create_articles_table',1),('2017_02_19_183902_create_categories_table',1),('2017_02_19_183911_create_role_users_table',1),('2017_02_20_145350_create_article_categories_table',1),('2017_04_25_100305_create_permission_role_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Dandre Beier','karianne48@example.com','$2y$10$HSNPAcXzgPZ4zoIrxP6TEuLNkmB13hp5ZR0WozWRPqhbQB2v5f8eO','XYjB6fS96C','2017-04-25 10:24:12','2017-04-25 10:24:12'),(2,'Karen Collins V','krajcik.jasper@example.net','$2y$10$DXxucRuVRBlhHrRnj234ueHBEIuBSP.65T..JhAXGwJunYyki.BlK','i1Lf7sxsU3','2017-04-25 10:24:13','2017-04-25 10:24:13'),(3,'Rupert Goyette','benny10@example.net','$2y$10$VIqwjkPErr1eL1GyXJZxouU2mb2boRkZ2J2c4.ANj1wewLVZzFJmC','vV3EBg7eTC','2017-04-25 10:24:13','2017-04-25 10:24:13'),(4,'Dangelo Hoeger','schuppe.parker@example.net','$2y$10$Ow6x1.M10e2b8etbEFfhjO6YM9yWHyRNJgVtHnr6sejx8WAmxzCgq','T7E1K0y5Y1','2017-04-25 10:24:13','2017-04-25 10:24:13'),(5,'Prof. Ethyl Jacobi DDS','barbara11@example.net','$2y$10$3tVR7JDRJEWb1k6LPNNelursSpUzSDcwubL4m0YmOCcoUuQZOQ/k.','EhsVu5Bs4H','2017-04-25 10:24:13','2017-04-25 10:24:13'),(6,'Mr. Cordell Legros','cleo46@example.net','$2y$10$WL9YXQWEk1VSY9wgncABtuuosWiXLgKxkxycHcbI6nVJk0yswZctG','rMVb82vcim','2017-04-25 10:24:13','2017-04-25 10:24:13'),(7,'Ms. Therese Leannon','raleigh.konopelski@example.com','$2y$10$hBPZLBKehfgG0XdMKwyeZ.eY5bhlfB4D3k.L3BlJ.C273gjOd6pg.','bR6RA1l5A6','2017-04-25 10:24:13','2017-04-25 10:24:13'),(8,'Leslie Jaskolski','branson50@example.com','$2y$10$OphjU33T7w8vpz82EgNO9e9xDBkm8kJYtcLSDNAa9ZT86TgJ71oC2','Ub398qnug0','2017-04-25 10:24:15','2017-04-25 10:24:15'),(9,'Tito Gusikowski Jr.','ray10@example.com','$2y$10$ZiUvyc5I46cHoqPsUjh/tuSaQj7R.wJ/j55GpRLVA.gLNMB44D7.S','UrMa8DJf4o','2017-04-25 10:24:15','2017-04-25 10:24:15'),(10,'Amie Wolf','padberg.imani@example.net','$2y$10$OlsHhz0fQHUSVNGqWuVlxuaE9FvnlidbCJCXQubMydokfxG3Z4iIu','0pgXSehCLR','2017-04-25 10:24:15','2017-04-25 10:24:15'),(11,'Monte Howell I','freda65@example.net','$2y$10$R/mYVXim8apG/pU3V.q/cetY8BjmDQXIY3YoVin/29YNpaOakFKQu','6JZsoe3TWr','2017-04-25 10:24:15','2017-04-25 10:24:15'),(12,'Lizeth Hartmann','hickle.jaquan@example.net','$2y$10$7aU.iTBeU2UnrH/Gr7EIB.pUUF2hHR8d/cANN42nhs.88cHVOmRum','zmJ3ExxoAe','2017-04-25 10:24:15','2017-04-25 10:24:15'),(13,'Darryl Kiehn Sr.','ebert.brant@example.net','$2y$10$RI/HEHEG1pu6RPoVym6oc.k3Sk7dJeadi6gIsvLN4aq.R9qdCQMoG','w2RWSsXB4e','2017-04-25 10:24:15','2017-04-25 10:24:15'),(14,'Mr. Kody Boyer PhD','desiree41@example.net','$2y$10$UR73mm26AzpBjh70Ft511eT33Yy72YQ35CePly0qlrqd8Y6Tf2u2e','fmLiLIGiWg','2017-04-25 10:24:15','2017-04-25 10:24:15'),(15,'Sedrick Shields PhD','else28@example.org','$2y$10$YNJf8r/N3URNWiuNtJ6VJ.sSvajap3GRZ3zyO4sOt7r9DXwRNx22a','UtIUio2rcG','2017-04-25 10:24:15','2017-04-25 10:24:15'),(16,'Bill Ortiz Jr.','emma.bode@example.net','$2y$10$1xYrjUsv4ejbtgoMwQzx..cZAMrp9lT2RPsDbg6BolkGxklvgWpra','eSZaTkflNd','2017-04-25 10:24:15','2017-04-25 10:24:15'),(17,'Geovanny Crist','heathcote.leatha@example.org','$2y$10$5XKnOzLaJIEPsXAr3KHQgu4SmHyeY/34tjbgLZ/zKoyJLUoPDUZ2q','mA12dq2Wbw','2017-04-25 10:24:15','2017-04-25 10:24:15'),(18,'Miss Violette Lebsack','jada29@example.com','$2y$10$M9itX/jI175JKEdHfPZmLOIuj8D4db89fYCQaQa1V7X9DrM07htUa','Bkx3GjQRvX','2017-04-25 10:24:15','2017-04-25 10:24:15'),(19,'Darwin Kovacek MD','jace99@example.com','$2y$10$Z88tKbuJO1pF9ytKioe1Res5r1iwwGTTBp/ZOBscaQXzuiMqBRu8q','fOo6iKLS3Q','2017-04-25 10:24:15','2017-04-25 10:24:15'),(20,'Donavon O\'Keefe','elta.feil@example.com','$2y$10$dV00HejIviQ01NBQNLZtO.CHQYnIPKnfpPxGwG5ZEBNRqZ6kteHkG','5rZCKpyedl','2017-04-25 10:24:15','2017-04-25 10:24:15'),(21,'Tiara Pouros','dratke@example.com','$2y$10$ivYxXJP47rRA3g6pL8hi/.dbjSrX4wuVxcOdU6Z.zdZyPtjultgZq','QRVKmDKNpv','2017-04-25 10:24:15','2017-04-25 10:24:15'),(22,'Mr. Filiberto Weimann DVM','general47@example.org','$2y$10$5DFibh3C2iT8nPfJjrPul.Lyfib8vCzrL9uL7ekBGiHRbdd7jvNsq','uwrsToTHeT','2017-04-25 10:24:15','2017-04-25 10:24:15'),(23,'Cole Herman','hilpert.karelle@example.com','$2y$10$OXGXLYkoaCeVPaFyQ2tMzuvcDCOYDJCVs1U1xoI6wy8hhUB8pKSzy','Ou4RXfrgaH','2017-04-25 10:24:15','2017-04-25 10:24:15'),(24,'Mrs. Gertrude Mills','conner.kuhlman@example.com','$2y$10$6LUUrcoxEpx5gqhanjm6VeePy4/4XQzSbYuhU23UeGQLKHgy3kmJ6','sSX4POW5y9','2017-04-25 10:24:15','2017-04-25 10:24:15'),(25,'Dr. Margarett Hagenes','jhoppe@example.net','$2y$10$7y8xqJSj65s1RJ2fdEw5YeFCqafep2olZJQzdli2ufiTTBN7X9yWG','9kIAOz5gig','2017-04-25 10:24:15','2017-04-25 10:24:15'),(26,'Prof. Mortimer Ferry','bschmeler@example.net','$2y$10$wrsJ9BAzXduHQvuiwtrGp.8ahzWg7sBft6kXeu9AuR0QFwOry5ziq','1Jh1Vaul53','2017-04-25 10:24:15','2017-04-25 10:24:15'),(27,'Scarlett Cole','rmcclure@example.com','$2y$10$Lzwg0stT2I1ARlRsIamitu9uCc4Sdfatm4YwLhfkNdZjiKqmBYFr.','G61FtUog7L','2017-04-25 10:24:15','2017-04-25 10:24:15'),(28,'Viola Russel','hgibson@example.net','$2y$10$2msYtYI80biGx26Iq5CJq.kXWA2IOmEdctK5lNjNtce1n2uLcpcte','XRmTzIjFQu','2017-04-25 10:24:15','2017-04-25 10:24:15'),(29,'Eli Homenick','larson.florence@example.net','$2y$10$tQetU9g.iw/4CrPRHwiEGuGtU.l1qFraHUVATmnS4lIpm1W4VjusO','64OCelv2FZ','2017-04-25 10:24:15','2017-04-25 10:24:15'),(30,'Anahi Watsica IV','amanda10@example.net','$2y$10$/cBZhkFNXtamh6FrvlnBL.IjbEufPEqOqP8Roie0aXWPYoGluMbM2','rGoXjIwjYH','2017-04-25 10:24:15','2017-04-25 10:24:15'),(31,'Mrs. Neha Sauer','feeney.melvin@example.com','$2y$10$tuXHi9Yq5nlQvhlYuRKMCup2rX6/0XhW5H5jEqrDTumIahyy3gFBO','ivASpEaB6r','2017-04-25 10:24:15','2017-04-25 10:24:15'),(32,'Travis Rosenbaum DDS','wtreutel@example.net','$2y$10$LEc4hOaqXlb5WInf.yM95u4Nf7bHeD4SbtKtuVZnuVDJ/I6kME/2q','H9CyiERos4','2017-04-25 10:24:15','2017-04-25 10:24:15'),(33,'Prof. Brennan Koelpin Jr.','kaci.fadel@example.net','$2y$10$nNXGj0KqZNfflkUo/TXPuOJWbkNbSPwLLTkryU/MlVi7RPofANYYC','PP8vuIjYnM','2017-04-25 10:24:15','2017-04-25 10:24:15'),(34,'Prof. Olin Prohaska','shaina55@example.org','$2y$10$UgH6.Q8fZBbS4Tm/S28OeOoz83OIeSMFyt8EbFQLM4VowHnob493y','2KRGR5vmqO','2017-04-25 10:24:15','2017-04-25 10:24:15'),(35,'Mohammad Welch','wade.stanton@example.com','$2y$10$HXOPth5H0v9DdO4ZZq1rSeWQwfP41a6rSGpsuOvh6HWS1pgn1rAD2','BWy491B80A','2017-04-25 10:24:15','2017-04-25 10:24:15'),(36,'Winston Stracke','hilpert.jesse@example.net','$2y$10$s09DtiMRyFyeEUh6IWwZee6SROZlBdXCGBwZXDK60mfVvNP1QASlG','5oisEa8q3x','2017-04-25 10:24:15','2017-04-25 10:24:15'),(37,'Jazmyn Mohr','goldner.elouise@example.net','$2y$10$RZmE/CcLqBfpH9sDm7uH7uvInRbbMuMAY.PAgkLd5YjRjT8lA5Tei','X73JSYu4T8','2017-04-25 10:24:15','2017-04-25 10:24:15'),(38,'Kane Wolf','elaina.sanford@example.net','$2y$10$bfS4qqq4Uz0FG4bwuBNtEuTNgrCTZ0q1BkkPVppzdCNAE6I3s3DPa','Zl0GaCs9Lg','2017-04-25 10:24:15','2017-04-25 10:24:15'),(39,'Reynold Botsford PhD','emard.emerson@example.com','$2y$10$uNXiwaLosNJFGxx0lm4Zr.CTjurK0yk9jZOUWToRVSKZvGBtYnN6O','RXO8Sh2Vzk','2017-04-25 10:24:15','2017-04-25 10:24:15'),(40,'Barton McKenzie','rodriguez.pinkie@example.org','$2y$10$zivr7jwSiy.pRUDShxX1z.1H.gw54pTDesEeGYJO0YnupoYjw0J7W','02yqzmPVOj','2017-04-25 10:24:15','2017-04-25 10:24:15'),(41,'Celine Leannon','ireichert@example.net','$2y$10$VQM20tbI0i7K5BtqeLqYjuc0PvJbUzJwc/uCEMoFrT5XxOswIkFY6','PaEkF37j2H','2017-04-25 10:24:15','2017-04-25 10:24:15'),(42,'Prof. Brycen Lynch I','sawayn.buford@example.com','$2y$10$oMad9b4yX6Ghhgah6LvRaeWUIkpUCvTJvH2YNKJcoIzdF7.ZSicSy','ef0wLdid2f','2017-04-25 10:24:15','2017-04-25 10:24:15'),(43,'Kelli Hessel','ebony.swift@example.com','$2y$10$/8676syDMy0RhXa6UUdfa.yQrc7qn4wbh864MYCJ6Zwvv.fUvViCS','Z1vX6B2vps','2017-04-25 10:24:15','2017-04-25 10:24:15'),(44,'Jeromy Ferry Jr.','frami.cornelius@example.com','$2y$10$EI205FerBM.jd6RqUBb5OOUKdbcEhv3ePsAAU79cpUa5ZQPbUvywC','HNqwzOCRRq','2017-04-25 10:24:15','2017-04-25 10:24:15'),(45,'Prof. Barrett Moen PhD','pacocha.jaydon@example.com','$2y$10$76vymOHR0rgQy.Niu2mKiey98SvS1wpWQ3BKzUR8/Ia92QOZuVV02','irCxtRayM4','2017-04-25 10:24:15','2017-04-25 10:24:15'),(46,'Alf Lakin','tleuschke@example.org','$2y$10$PI6DtiHTBydqyriKGvmGNu/QQd4Lsanme9GqnHgAjkwOnfM76w3eq','HajGMfUhDK','2017-04-25 10:24:15','2017-04-25 10:24:15'),(47,'Andreane Medhurst','ambrose.ebert@example.org','$2y$10$Jgkfybjo9.IkVjVYxYLakeGTK0cZhZ5/gTCdQ8B3qGfk/w/x6Ny.i','yQErpRE1j9','2017-04-25 10:24:15','2017-04-25 10:24:15'),(48,'Mr. Shawn Blanda','phyllis.harber@example.org','$2y$10$TnPvsGDNEjwe8YIv9Y96Tutg9qfo4JWAq3dOwiRyp2sotcAfvB/3W','OBcAeigyUU','2017-04-25 10:24:16','2017-04-25 10:24:16'),(49,'Saige Jacobs','daron.strosin@example.net','$2y$10$Pn6WyUMmrJapTvYMBLGHn.BqgcO87yfGKSShIsk.mgAH5rPm4W072','M2WSnlnQn3','2017-04-25 10:24:16','2017-04-25 10:24:16'),(50,'Ettie Pouros','nolan.violette@example.net','$2y$10$fruimLxm4TjtZJjUXTfWe.CcIL78AFu/PQwsk6esSxbXAoLJyoI1.','VuYbML9fmw','2017-04-25 10:24:16','2017-04-25 10:24:16');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-25 12:38:47
